package com.example.restfulapi.services;

import com.example.restfulapi.dtos.SellRequestDTO;
import com.example.restfulapi.dtos.SellResponseDTO;
import com.example.restfulapi.factories.SellFactory;
import com.example.restfulapi.models.SellModel;
import com.example.restfulapi.repositories.SellRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class SellService {

    @Autowired
    SellRepository repository;

    public SellResponseDTO create(SellRequestDTO request, UUID id){
        SellModel sell = new SellModel();
        BeanUtils.copyProperties(request, sell);
        sell.setProductID(id);

        return SellFactory.Create(repository.save(sell));
    }
    public SellResponseDTO getById(UUID id){

        Optional<SellModel> result = repository.findById(id);

        return result.isEmpty() ? null : SellFactory.Create(result.get());
    }
    public Page<SellModel> getAll(Pageable pageable){
        Page<SellModel> sell = repository.findAll(pageable);
        return sell;
    }

}
