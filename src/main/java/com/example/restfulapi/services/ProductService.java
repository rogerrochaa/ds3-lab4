package com.example.restfulapi.services;

import com.example.restfulapi.dtos.ProductRequestDTO;
import com.example.restfulapi.dtos.ProductResponseDTO;
import com.example.restfulapi.factories.ProductFactory;
import com.example.restfulapi.models.ProductModel;
import com.example.restfulapi.repositories.ProductRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public ProductResponseDTO create(ProductRequestDTO request){

        ProductModel product = new ProductModel();

        BeanUtils.copyProperties(request, product);

        return ProductFactory.Create(productRepository.save(product));
    }
    public ProductResponseDTO getById(UUID id){

        Optional<ProductModel> result = productRepository.findById(id);

        return result.isEmpty() ? null : ProductFactory.Create(result.get());
    }
    public Page<ProductModel> getAll(Pageable pageable){
        Page<ProductModel> products = productRepository.findAll(pageable);
        return products;
    }
    public ProductResponseDTO update(ProductRequestDTO dto, UUID id){

        Optional<ProductModel> result = productRepository.findById(id);

        if(result.isEmpty()) return null;

        result.get().setName(dto.name());
        result.get().setPrice(dto.price());
        result.get().setQuantity(dto.quantity());

        ProductModel saved = productRepository.save(result.get());

        return ProductFactory.Create(saved);
    }
    public ProductResponseDTO updateQnt(Integer quantity, UUID id){

        Optional<ProductModel> result = productRepository.findById(id);

        if(result.isEmpty()) return null;

        result.get().setQuantity(quantity);

        ProductModel saved = productRepository.save(result.get());

        return ProductFactory.Create(saved);
    }
    public boolean delete(UUID id){

        Optional<ProductModel> result = productRepository.findById(id);

        if(result.isEmpty()) return false;

        productRepository.delete(result.get());

        return true;
    }
}
