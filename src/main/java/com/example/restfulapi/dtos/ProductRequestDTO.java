package com.example.restfulapi.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;

public record ProductRequestDTO(@NotBlank String name, @Positive Double price, @Positive Integer quantity) {
}
