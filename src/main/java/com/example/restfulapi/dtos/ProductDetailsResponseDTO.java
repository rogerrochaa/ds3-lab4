package com.example.restfulapi.dtos;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

public record ProductDetailsResponseDTO(UUID id, String name, Double price, LocalDateTime createdAt, LocalDateTime updatedAt) implements Serializable {
}
