package com.example.restfulapi.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;

import java.util.UUID;

public record SellRequestDTO(@Positive Integer quantity) {
}
