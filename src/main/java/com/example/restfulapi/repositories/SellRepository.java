package com.example.restfulapi.repositories;

import com.example.restfulapi.models.SellModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SellRepository extends JpaRepository<SellModel, UUID> {
}
