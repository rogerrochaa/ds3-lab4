package com.example.restfulapi.models;

import jakarta.persistence.*;

import java.util.UUID;

@Entity
@Table(name = "TB_SELLS")
public class SellModel extends AbstractEntityModel{

    @Column(name = "ID_PRODUCT")
    private UUID productID;
    @Column(name = "VL_QUANTITY")
    private Integer quantity;

    public UUID getProductID() {
        return productID;
    }

    public void setProductID(UUID productID) {
        this.productID = productID;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
