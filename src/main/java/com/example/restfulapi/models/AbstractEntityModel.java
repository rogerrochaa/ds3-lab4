package com.example.restfulapi.models;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractEntityModel {
     AbstractEntityModel(){
        LocalDateTime now = LocalDateTime.now();
        CreatedAt = now;
        UpdatedAt = now;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "ID")
    private UUID Id;
    @Column(name = "DT_CREATED_AT")
    private final LocalDateTime CreatedAt;
    @Column(name = "DT_UPDATED_AT")
    private LocalDateTime UpdatedAt;

    public UUID getId() {
        return Id;
    }

    public void setId(UUID id) {
        Id = id;
    }

    public LocalDateTime getCreatedAt() {
        return CreatedAt;
    }

    public LocalDateTime getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        UpdatedAt = updatedAt;
    }

}
