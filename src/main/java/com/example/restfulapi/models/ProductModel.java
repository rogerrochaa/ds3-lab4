package com.example.restfulapi.models;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name="TB_PRODUCTS")
public class ProductModel extends AbstractEntityModel {
    @Column(name = "NM_PRODUCT")
    private String name;
    @Column(name = "VL_PRICE")
    private Double price;
    @Column(name = "VL_QUANTITY")
    private Integer quantity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {this.quantity = quantity;}


}
