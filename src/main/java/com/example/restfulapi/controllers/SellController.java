package com.example.restfulapi.controllers;

import com.example.restfulapi.dtos.ProductResponseDTO;
import com.example.restfulapi.dtos.SellRequestDTO;
import com.example.restfulapi.dtos.SellResponseDTO;
import com.example.restfulapi.models.ProductModel;
import com.example.restfulapi.models.SellModel;
import com.example.restfulapi.repositories.ProductRepository;
import com.example.restfulapi.services.ProductService;
import com.example.restfulapi.services.SellService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/sells")
public class SellController {

    @Autowired
    SellService sellService;
    @Autowired
    ProductService productService;

    @PostMapping("/{id}")
    public ResponseEntity<SellResponseDTO> create(@PathVariable(value = "id") UUID id, @RequestBody @Valid SellRequestDTO dto) throws Exception {
        var product = productService.getById(id);

        if (product == null) {
            throw new Exception("Produto com ID " + id + " não encontrado");
        }

        if (product.getQuantity() < dto.quantity()) {
            throw new Exception("Estoque insuficiente para o produto " + product.getId());
        }


        SellResponseDTO response = sellService.create(dto, id);


        if (response != null) {
            Integer newQuantity = product.getQuantity() - dto.quantity();
            productService.updateQnt(newQuantity, product.getId());
            response.add(linkTo(methodOn(ProductController.class).get(response.getId())).withSelfRel());
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } else {
            // Handle unsuccessful sale creation (e.g., log error or return error response)
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    @GetMapping
    public ResponseEntity<Page<SellModel>> getAll(@PageableDefault(page = 0, size = 3) Pageable pageable){

        return ResponseEntity.status(HttpStatus.OK).body(sellService.getAll(pageable));
    }}


