package com.example.restfulapi.factories;

import com.example.restfulapi.dtos.SellResponseDTO;
import com.example.restfulapi.models.SellModel;
import org.springframework.beans.BeanUtils;

public class SellFactory {
    public static SellResponseDTO Create(SellModel sell){
        SellResponseDTO dto = new SellResponseDTO();
        BeanUtils.copyProperties(sell, dto);
        return dto;
    }
}

