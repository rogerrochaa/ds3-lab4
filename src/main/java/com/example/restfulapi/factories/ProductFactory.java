package com.example.restfulapi.factories;

import com.example.restfulapi.dtos.ProductResponseDTO;
import com.example.restfulapi.models.ProductModel;
import org.springframework.beans.BeanUtils;

public class ProductFactory {
    public static ProductResponseDTO Create(ProductModel product){
        ProductResponseDTO dto = new ProductResponseDTO();
        BeanUtils.copyProperties(product, dto);
        return dto;
    }
}
